package com.crdif.openapi.app.parsexml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.dom4j.Node;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.crdif.openapi.app.recovery.RecoveryJcmsData;

	public class MemberLocalisation {
private String  path;
private Logger logger=Logger.getLogger(this.getClass());
	public MemberLocalisation(String path) {
				this.path=path;
}
	/**
	 * Lecture d'un fichier
	 * XMl et retourne un nodeList
	 * @return
	 */
	public NodeList readXmlFile() {
		
	 DocumentBuilderFactory factory;
	  Document document = null;
	 
	  try {
		  factory=DocumentBuilderFactory.newInstance();
		  
		  DocumentBuilder builder = factory.newDocumentBuilder();	
		 
		  document= builder.parse(new File(this.path));
		 
		  
}
	catch (final ParserConfigurationException e) {
    //e.printStackTrace();
	logger.error(e+"   :"+"erreur de parsing du fichier xml");
	System.out.println("erreur de parsing du fichier xml");
	System.exit(-1);
}
catch (final SAXException e) {
  // e.printStackTrace();
	logger.error(e+"   :"+"erreur de parsing du fichier xml");
	System.out.println("erreur de parsing du fichier xml");
	System.exit(-1);
	}
	catch (final IOException e) {
   // e.printStackTrace();
		logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
		System.out.println("erreur de lecture du fichier de configuration");
		System.exit(-1);
}
Element racine=document.getDocumentElement();
NodeList node=racine.getChildNodes();
	return node;
}
	/**
	 * fonction retournant l'id grace au login fournit en 
	 * parametre
	 * @param login
	 * @return
	 */
public String getIdLocalisationByLogin(String login){
	List<String>loginList=this.DeleteDoublonsLogin(getAllIdOrLoginFromLocalisationFile("login"));
	List<String>idList=this.DeleteDoublons(this.getAllIdOrLoginFromLocalisationFile("idLocalisation"));
	Map<String,String>mapInfos=new HashMap<String,String>();
	for(int i=0;i<loginList.size();i++) {
		
		mapInfos.put(loginList.get(i), idList.get(i));
	}
	return mapInfos.get(login);
	
}





/**
 * retourne la liste des index des anciennes modif membres
 *  n'ont pris en charges(administrateur par exemple)
 * @return
 */
public  List<Integer> IndexDoublons(){
	List<String>loginList=this.getAllIdOrLoginFromLocalisationFile("login");
	List<Integer> index=new ArrayList<Integer>();
	List<String> lse=new ArrayList<String>();
	
	for(int i=0;i<loginList.size();i++) {
		if(!(lse.contains(loginList.get(i)))){
			
			lse.add(loginList.get(i));
		}
		else {
			index.add(i);
		}
	}
	return index;
	
	
}


/**
 * supprime les doublons 
 * selon les indice fournies
 * @param lse
 * @return
 */

public List<String> DeleteDoublons(List<String> lse){
	List<String> del= new ArrayList<String>();
	for(Integer i:IndexDoublons()) {
		int j= (int)i;
		del.add(lse.get(j));
	}
	lse.removeAll(del);
	return lse;
}

/**
 * retourne la liste des logins sans doublons
 * @param lse
 * @return
 */
public List<String> DeleteDoublonsLogin(List<String> lse){
	List<String> newList=new ArrayList<String>();
	for(int i=0;i<lse.size();i++) {
		if(!(newList.contains(lse.get(i)))){
			
			newList.add(lse.get(i));
		}
	}
	
	return newList;
	
}




	
	/**
	 * 
	 * @return
	 */
	
	/**
	 * retourne la
	 * liste des logins 
	 * ou des Id
	 * du fichier
	 * MemberXml
	 * @param choice
	 * @return
	 */
	public List<String> getAllIdOrLoginFromMemberFile(String choice) {
		NodeList node= this.readXmlFile();
		List<String> liste = new ArrayList<String>();
		for(int i=0;i<node.getLength();i++) {
		  if(node.item(i).getNodeType()==Node.ELEMENT_NODE) {
			final	Element data=(Element) node.item(i);
			NodeList fields = data.getChildNodes();
			switch(choice) {
			 case "id":
				 if(!(liste.contains(data.getAttribute("id"))))
				liste.add(data.getAttribute("id"));
					break;
		    case"name":
				for(int j=0;j<fields.getLength();j++) {
					if(fields.item(j).getNodeType()==Node.ELEMENT_NODE) {
						Element field=(Element) fields.item(j);
						if(field.getAttribute("name").equals("name")) {
			    	liste.add(field.getTextContent());
						
					
						}
					}
			
				}
				break;
		    case"login":
				for(int j=0;j<fields.getLength();j++) {
					if(fields.item(j).getNodeType()==Node.ELEMENT_NODE) {
						Element field=(Element) fields.item(j);
						if(field.getAttribute("name").equals("login")) {
			    if(!(liste.contains(field.getTextContent()))
			    		&& ((!(field.getTextContent().equals("client")))
			    		
			    		&&	(!((field.getTextContent()).equals("admin")))
			    		)
			    		)
			    	liste.add(field.getTextContent());
						
					
						}
					}
			
				}
				break;
		default:;
		}
			
			
	}
		 
}
		
		
		
return liste;
}
	/**
	 * retourne la liste
	 * les logins 
	 * ou des Ids 
	 *  dans le 
	 *  XmlLocalisation
	 * @return une liste de IdLocalisation
	 */
	public List<String> getAllIdOrLoginFromLocalisationFile(String choice){
		
		NodeList node=this.readXmlFile();
		List<String> liste=new ArrayList<String>();
		for(int i=0;i<node.getLength();i++) {
			  if(node.item(i).getNodeType()==Node.ELEMENT_NODE) {
				final	Element data=(Element) node.item(i);
				NodeList fields = data.getChildNodes();
				if(choice.equals("idLocalisation")) {
					
					for(int j=0;j<fields.getLength();j++) {
						if(fields.item(j).getNodeType()==Node.ELEMENT_NODE) {
							Element field=(Element) fields.item(j);
						if(field.getAttribute("name").equals("author")) {
							
								liste.add(data.getAttribute("id"));
							
						}
						
						}
						
						}
			
				}
				else if(choice.equals("login")||choice.equals("id")) {
					for(int j=0;j<fields.getLength();j++) {
						if(fields.item(j).getNodeType()==Node.ELEMENT_NODE) {
							Element field=(Element) fields.item(j);
						if(field.getAttribute("name").equals("author")) {
							switch(choice) {
								case "login":
									
									liste.add(field.getAttribute("login"));
									break;
								case"id":
									liste.add(field.getAttribute("id"));
									break;
								default:;
								}
						}
					}
			}
		}
				
				
				
				else {
					
					for(int j=0;j<fields.getLength();j++) {
						if(fields.item(j).getNodeType()==Node.ELEMENT_NODE) {
							Element field=(Element) fields.item(j);
						
							switch(choice) {
							case "Alogin": if(field.getAttribute("name").equals("author")) {
								liste.add(field.getAttribute("login"));}
								break;
								case "time":
									if(field.getAttribute("name").equals("mdate")) {
									liste.add(field.getAttribute("time"));}
									break;
								case"title":
									if(field.getAttribute("name").equals("title")) {
									liste.add(field.getTextContent());}
									break;
								default:;
								}
						}
					}
			
					
					
					
				}
				
				
						
				
				
	}
}
		
return liste;
	
	}
	/**
	 * 
	 * @param absolutePath
	 * @return
	 */
	public static String  getParam(String absolutePath) {
		MemberLocalisation member;
		String attrTotal=null;
		NodeList datas;
		member=new MemberLocalisation(absolutePath);
		 datas= member.readXmlFile();
		for(int i=0;i<datas.getLength();i++) {
		 if(datas.item(i).getNodeType()==Node.ELEMENT_NODE) {
			final	Element data=(Element) datas.item(i);
			if(data.getNodeName().equals("pager")) {
			attrTotal =	data.getAttribute("total");
			}
			
		}
	}
return attrTotal;
		
}
	
	/**
	 * fonction retournant 
	 * le nombre total de 
	 * d'element
	 * @return
	 * @throws IOException 
	 */
	public static String getPagerAttrTotal(String choice,String fileConf) throws IOException {
		RecoveryJcmsData dat=new RecoveryJcmsData(fileConf);
		String attrTotal=null;
		String str;
		switch(choice) {
		case "Member":
			str=dat.createFile("Member.xml", "Member");
		attrTotal=getParam(str);
		break;
		case "Localisation":
			str=dat.createFile("Localisation.xml", "Localisation");
			attrTotal=getParam(str);
			break;
		default:;
		}
	return attrTotal;
}

	
	

	
}
	
	
	
	
	