package com.crdif.openapi.app.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigurationJcms {
	private Logger logger=Logger.getLogger(this.getClass());
	private String fileName;
	public  ConfigurationJcms(String fileName) {
this.fileName=fileName;
	}
	Properties properties=new Properties();
	public Properties getConfigurationFileElement() throws IOException {
		try (FileInputStream input = new FileInputStream(fileName)){
			
			try {
				properties.load(input);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e+ "    :"+"le programme a rencontré une erreur lors de la lecture du fichier de configuration"+"  "+ fileName);
				System.exit(-1);
			}
		}
		
		
		return properties;
	
	}
	
	   
	
}
