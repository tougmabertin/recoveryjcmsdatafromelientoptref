package com.crdif.openapi.app.recovery;
				
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import com.crdif.openapi.app.configuration.ConfigurationJcms;
import com.crdif.openapi.app.parsexml.MemberLocalisation;
import com.jalios.rest.client.ClientSession;
import com.jalios.rest.client.ClientUtil;
import com.jalios.rest.client.DataElement;
import com.jalios.rest.client.JcmsApp;
import com.jalios.rest.client.JcmsResource;
import com.jalios.rest.client.RestException;
import com.jalios.rest.client.authentication.BasicAuthentication;
				
	public class RecoveryJcmsData {
	protected JcmsApp jcms;
	protected ClientSession session;
	private String fileConf;
	protected RecoveryJcmsData recovery;
	protected String str =null;
	protected MemberLocalisation member;
	protected boolean isLetterOnly=true;
	protected String parameter="Member?pageSize=";	
	private Logger logger=Logger.getLogger(this.getClass());
		/**
		 * constructeur
		 * @param fileConf
		 * @throws IOException 
		 */
	 public RecoveryJcmsData(String fileConf) throws IOException {
					// TODO Auto-generated constructor stub
			this.fileConf=fileConf;
		this.prepareAuthentificationStuff();
}
				/**
				 * Authentification basic
				 * @throws IOException 
				 */
		public void prepareAuthentificationStuff()  {
			try {
				session=new ClientSession(new ConfigurationJcms(this.fileConf).
							 getConfigurationFileElement().getProperty("baseref"));
				session.setAuthentication(new BasicAuthentication
						 (new ConfigurationJcms(this.fileConf).
								 getConfigurationFileElement().getProperty("loginElien"),
								 new ConfigurationJcms(this.fileConf).
								 getConfigurationFileElement().getProperty("passwdElien")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				
				logger.error(e);
			}
			
}
			
					 /**
					  * recup�re le champs
					  *  texte grace au login membre et
					  * a l'attribut
					  * sur le site JCMS distant
					  * @param login
					  * @param atribut
					  * @return
					  */
		public String getInfos(String login,String atribut) {
			String str = null;
						switch(atribut) {
						
						case "login":
							 str=this.getDataElementFromLogin(login).getFieldText("login");
								break;
						case "email":
							 str=this.getDataElementFromLogin(login).getFieldText("email");
							 	break;
						case "phone":
							 str=this.getDataElementFromLogin(login).getFieldText("phone");
								break;
						case "id":
							str=this.getDataElementFromLogin(login).getId();
							break;
						case "photo":
							 str=this.getDataElementFromLogin(login).getFieldText("photo");
								break;
						case "name":
							str=this.getDataElementFromLogin(login).getFieldText("name");
								break;
						case "firstName":
							str=this.getDataElementFromLogin(login).getFieldText("firstName");
							break;
						default:
							;
			}
return str;
}
		
		
		
					/**
					 * retourne une liste
					 * des membres
					 * actifs
					 * @param lse1
					 * @param lse2
					 * @return
					 */
		public static List<String> getMemberActif(List<String> lse1,List<String> lse2){
						for(int j=0;j<lse2.size();j++) {
						if(lse1.contains(lse2.get(j))) {
							lse1.remove(lse2.get(j));
				}
}
						
return lse1;
}
				
				/**
				 *  retourne une liste
				 *  des 
				 *  comptes membre desactiv�s.
				 * @param pathJson
				 * @return
				 * @throws IOException 
				 */
		
		public List<String> unknownLogin() {
			List<String> liste=new ArrayList<String>();
			
			JcmsApp jcms=new JcmsApp(session);
			JcmsResource memberResource = null;
			try {
				parameter+=MemberLocalisation.getPagerAttrTotal("Member",this.fileConf);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e);
			}
			try {
				member=new MemberLocalisation(this.createFile("Member.xml", parameter));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e);
			}
			List<String> lse=member.getAllIdOrLoginFromMemberFile("login");
	
			for(int i=0;i<lse.size();i++) {
			 try {
				  memberResource=jcms.getMember(lse.get(i));			
				} catch (RestException e) {
		liste.add(lse.get(i));
             }
		    try {new DataElement(ClientUtil.
					 getFirstElement(memberResource,"/data"),session);
					} catch (JDOMException e) {
						// TODO Auto-generated catch block
					//e.printStackTrace();
						logger.error(e+"  :"+" erreur d'acc�s � la ressource membre");
						System.exit(-1);
			}
     }
return liste;
}
								
							/**
							 * Methode permettant d'acceder au site jalios distant						
							 * @param login
							 * @return
							 */
	   public DataElement getDataElementFromLogin(String login) {
				JcmsApp jcms=new JcmsApp(this.session);
				JcmsResource memberResource = null;
				DataElement memberData = null;
		try {
			
			memberResource=jcms.getMember(login);
		
									
		} catch (RestException e) {
						// TODO Auto-generated catch block
				
								//e.printStackTrace();
				logger.error(e+"  :"+"Une erreur rencontr�e lors de la remont� d'informations depuis le site elien"+"   :"+  login);
				System.exit(-1);
		}
		try {memberData=new DataElement(ClientUtil.
				getFirstElement(memberResource,"/data"),session);
			} catch (JDOMException e) {
				// TODO Auto-generated catch block
					//e.printStackTrace();
				logger.error(e+"  :"+" erreur d'acc�s � la ressource membre");
				System.exit(-1);
				}
return memberData;
}
	   
	   
/**
 * prend en param�tre un id, acc�de
 * au site distant et retourne 
 * une information sur sa localisation
 * @param id
 * @return
 */
	   public DataElement getDataElementFromIdLocalisation(String id) {
				JcmsApp jcms=new JcmsApp(this.session);
				JcmsResource data = null;
				DataElement memberData = null;
				try {
					data=jcms.getData(id);
				}
				catch(RestException e) {
					logger.error(e+"  :"+" erreur d'acc�s � la ressource membre via  REST");
					System.exit(-1);
				}
				try {memberData=new DataElement(ClientUtil.
						getFirstElement(data,"/data"),session);
					} catch (JDOMException e) {
						// TODO Auto-generated catch block
						logger.error(e+"  :"+" erreur d'acc�s � la ressource membre");
						System.exit(-1);
						}
				return memberData;
				
			}

			/**
			 * 
			 * 
			 * @param id
			 * @param atribut
			 * @return
			 */
			public String getLocalisationInfos(String id,String atribut) {
				switch(atribut) {
				  case "zone":
						str=this.getDataElementFromIdLocalisation(id).getFieldText("zone");
						break;
			   case "login":
						str=this.getDataElementFromIdLocalisation(id).getFieldText("login");
						break;
				case "site":
					 str=this.getDataElementFromIdLocalisation(id).getFieldText("site");
						break;
				case "bureau":
					 str=this.getDataElementFromIdLocalisation(id).getFieldText("bureau");
						 break;
				case "id":
					str= this.getDataElementFromIdLocalisation(id).getId();
					break;
				case "etage":
					 str=this.getDataElementFromIdLocalisation(id).getFieldText("etage");
						break;
				default:
					;
	}
   return str;
 }

	/**
	 * 
	 * @param nameFile
	 * @param param
	 * @return
	 * @throws IOException 
	 */
			public  String createFile(String nameFile,String param) throws IOException {
				String dirTmp=new ConfigurationJcms(this.fileConf).
						 getConfigurationFileElement().getProperty("dir");
				 File file = null;
				 JcmsResource data = null;
				 JcmsApp jcms=new JcmsApp(this.session);	
					try {
						data=jcms.getData(param);
					}
					catch(RestException e) {
						//e.printStackTrace();
						logger.error(e+"  :"+" erreur d'acc�s � la ressource membre via  REST");
						System.exit(-1);
						
					}
					try {
                       file = new File(dirTmp+File.separator+nameFile);
					   if (!file.exists()) {
					     file.createNewFile();
					   }
  FileOutputStream out=new FileOutputStream(file);
		Writer  writer=new OutputStreamWriter(out,"UTF-8");
		writer.write(data.getEntityText());
		writer.close();
					  
     } catch (IOException e) {
					//   e.printStackTrace();
    	 logger.error(e+ "    :"+" erreur de lecture du fichier de configuration");
			System.exit(-1);
     }
return file.getAbsolutePath();
}
			
			
}
