package com.crdif.openapi.app.main;
import com.crdif.openapi.app.configuration.ConfigurationJcms;
import com.crdif.openapi.app.Bd.DaoRemoteJcms;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import org.apache.log4j.LogManager;
import com.crdif.openapi.app.Bd.Bd;



public class MainClass {
	 static org.apache.log4j.Logger logger=LogManager.getLogger(MainClass.class);
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException   {
		// TODO Auto-generated method stub
		String fileConf= args[0];	
	try {
	Bd bd=new Bd(fileConf);
DaoRemoteJcms dao=new DaoRemoteJcms(bd,fileConf);
dao.miseAjour();
new File(new ConfigurationJcms(fileConf).
		getConfigurationFileElement().getProperty("dir")+File.separator+"Member.xml").deleteOnExit();
new File(new ConfigurationJcms(fileConf).
		getConfigurationFileElement().getProperty("dir")+File.separator+"Localisation.xml").deleteOnExit();
	}
	catch(IOException e) {
		logger.error(e+ "    :"+"le programme a rencontr� une erreur lors de la lecture du fichier de configuration"+"  "+fileConf);
		System.exit(-1);
	}
catch(NullPointerException e) {
	e.printStackTrace();
	logger.error(e+"  :"+"vous essayez  d'acceder � une  ressource inexistante");
	System.exit(-1);
	
	}
	System.exit(0);
	}

}
