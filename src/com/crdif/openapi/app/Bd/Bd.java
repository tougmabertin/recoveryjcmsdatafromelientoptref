package com.crdif.openapi.app.Bd;

import com.crdif.openapi.app.configuration.ConfigurationJcms;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;


public class Bd {
	private Logger logger=Logger.getLogger(this.getClass());
	private String fileName;
	
	public Bd(String fileName)  {
		this.fileName=fileName;
		try {
		Class.forName(new ConfigurationJcms(this.fileName).getConfigurationFileElement().getProperty("driver"));
		}
		catch( ClassNotFoundException e) {
			logger.error(e+"    :"+ "erreur de chargement du dirver de jdbc");
			System.exit(-1);
		}
		catch(IOException e) {
			//logger.error(e);
			logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
			System.exit(-1);
		}
}

public Connection getConnection() throws SQLException, IOException {
	return DriverManager.getConnection(
			new ConfigurationJcms(this.fileName).getConfigurationFileElement().getProperty("url"),
			new ConfigurationJcms(this.fileName).getConfigurationFileElement().getProperty("login"),
			new ConfigurationJcms(this.fileName).getConfigurationFileElement().getProperty("passwd"));
	
}

}
