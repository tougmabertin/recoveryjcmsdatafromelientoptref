package com.crdif.openapi.app.Bd;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.crdif.openapi.app.parsexml.MemberLocalisation;
import com.crdif.openapi.app.recovery.RecoveryJcmsData;



public class DaoRemoteJcms {
private Bd bd;
private String fileConf;
protected String telephone, site,bureau, etage,id,name,email,photo,zone;
protected MemberLocalisation member,localisation;
protected RecoveryJcmsData data;
protected String parameterMember="Member?pageSize=";
protected String parameterLocalisation="Localisation?pageSize=";
private Logger logger=Logger.getLogger(this.getClass());

public DaoRemoteJcms(Bd bd,String fileConf) {
	this.bd=bd;
	this.fileConf=fileConf;
}


public Connection getConnexion() throws SQLException, IOException {
	return bd.getConnection();
	
}
public List<String> getListeToUpper(List<String> liste){
	List<String> lse=new ArrayList<String>();
	for(int i=0;i<liste.size();i++) {
		lse.add(liste.get(i).toUpperCase());
	}
	return lse;
}



	
public void  miseAjour()  {
	 PreparedStatement prepareStatement;
	try {
		data= new RecoveryJcmsData(this.fileConf);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
		System.exit(-1);
	}
	List<String>lse2=data.unknownLogin();
	
	try {
		parameterMember+=MemberLocalisation.getPagerAttrTotal("Member",this.fileConf);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
		System.exit(-1);
	}
	try {
		parameterLocalisation+=MemberLocalisation.getPagerAttrTotal("Localisation",this.fileConf);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
		System.exit(-1);
	}
	try {
		member=new MemberLocalisation(data.createFile("Member.xml",parameterMember));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
		System.exit(-1);
	}
	try {
		localisation=new MemberLocalisation(data.createFile("Localisation.xml", parameterLocalisation));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error(e);
	}
	List<String> lse1=member.getAllIdOrLoginFromMemberFile("login");
	logger.info(" nombre total de membres:  "+"   "+lse1.size());
	logger.info(lse2.size()+"   "+" membres desactiv�s");
	List<String> lse=RecoveryJcmsData.getMemberActif(lse1,lse2);
	logger.info(lse.size()+"   "+"  membres actifs");
	List<String>logins=localisation.getAllIdOrLoginFromLocalisationFile("login");
	//List<String>logins=localisation.getAllIdOrLoginFromLocalisationFile("login");
	logger.info(logins.size()+"  membres ayant renseign�s les informations sur leur localisation");
String	sql="UPDATE personnes SET per_tel=?, per_site=?,per_etage=?,per_bureau=?, per_zone=? WHERE upper(per_login)=upper(?)";
	try(Connection connexion=getConnexion() ){
		Logger logger=Logger.getLogger(this.getClass());
		logger.info("Connexion � la base de donn�es");
	//	PreparedStatement prepareStatementSelect=connexion.prepareStatement(sqlSelect);
		 prepareStatement=connexion.prepareStatement(sql);
		 logger.info("connexion reussie");
		 for(int i=0;i<lse.size();i++) {
			telephone=data.getInfos(lse.get(i), "phone");
			 if(logins.contains(lse.get(i))) {
			 id=localisation.getIdLocalisationByLogin(lse.get(i));
			 site=data.getLocalisationInfos(id, "site");
			 etage=data.getLocalisationInfos(id, "etage");
			 bureau=data.getLocalisationInfos(id, "bureau");
			 zone=data.getLocalisationInfos(id, "zone");
			
			}
			 else {
					site=null;
					etage=null;
					bureau=null;
					zone=null;
				}
			 try {
				
			 prepareStatement.setString(1, telephone);
			 prepareStatement.setString(2,  site);
			 prepareStatement.setString(3, etage);
			 prepareStatement.setString(4,  bureau);
			 prepareStatement.setString(5, zone);
			prepareStatement.setString(6, lse.get(i));
				 
			 }
			
			 catch(NullPointerException ex) {
				logger.error(ex+"  :"+"vous essayez  d'acceder � valeur nulle");
				System.exit(-1);
				//continue;
				
			 }
			 prepareStatement.executeUpdate();
			 logger.info("mise � jour des informations du membre   "+"   "+lse.get(i));
				
		 }			
	
		 connexion.close();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		logger.error(e+"     :"+"erreur de connexion � la base de donn�es");
		System.exit(-1);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error(e+ "    :"+"erreur de lecture du fichier de configuration");
		System.exit(-1);
	}
	
	
	
}


	
}