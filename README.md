# RecoveryJcmsDataFromElienToPTREF
**Développement d’une Interface java de mise à jour d’une base de données:** 

Cette interface développée en java permet de d’interfacer un intranet du conseil
régional d’île de France et une base de données existante afin de mettre à jour
des informations renseigner( localisation , numéro d’étage , numéro de bureau, numéro de téléphone....)
dans l’intranet par les salariés de la région .
Technologies : REST, XML, JCMS(Jalios), Open API , Java, JDBC, CSV, Powershell, Eclipse.